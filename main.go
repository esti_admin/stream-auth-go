package main

import (
	"fmt"
  "strconv"
  "time"
  "strings"
  "os"
  "encoding/json"
  "github.com/go-redis/redis"
  "github.com/valyala/fasthttp"
  "gopkg.in/yaml.v2"
  "io/ioutil"
  "math/rand"
  log "github.com/sirupsen/logrus"
)

// Redis keys
const PublishEventsPipe = "events_pipe"
const IpBlockedKey  = "blocked_ip"
const ConnBlockedKey  = "blocked_conn"
const ActiveKey     = "active_user_ids"
const UniqueIpKey   = "active_ips"

const CleanupProbability  = 0.05  // probability of performing sorted sets cleanup
const CleanupOlderThan    = 60*60 // seconds ; cleanup members older than given for sorted sets

// Configuration for IP sets
const IPSET_PREFIX = "ipset:"     // ipset:<user_id>
const IPSET_TTL = 3 * 60 * 60     // 3 hours
const CONNSET_PREFIX = "connset:" // connset:<user_id>:<ip>
const CONNSET_TTL = 60 * 60       // 1 hour
const CONNHITS_PREFIX = "connhits:"
const CONNHITS_TTL = 60           // 1 hour
const CONNHITS_TRESHOLD = 3       // consider established connection after 10 hits
const SET_TTL = 60 * 60           // 1 hour

type conf struct {
    UserLimit   int `yaml:"user_limit"`
    IpLimit     int `yaml:"ip_limit"`
    ConnLimit   int `yaml:"conn_limit"`
    TimeFrame   int `yaml:"frame"`
}

// Read YAML config 
func (c *conf) readConf() *conf {

    yamlFile, err := ioutil.ReadFile("config.yml")
    if err != nil {
        log.Printf("yamlFile.Get err #%v ", err)
    }
    err = yaml.Unmarshal(yamlFile, c)
    if err != nil {
        log.Fatalf("Yaml unmarshal: %v", err)
    }

    return c
}

// Config available globally
var Config conf

// Global Redis client (maybe use pool later)
var RedisClient = redis.NewClient(&redis.Options{
	Addr:     "localhost:6379",
	Password: "", // no password set
	DB:       0,  // use default DB
})

type AuthHandler struct { }

func (h *AuthHandler) HandleRequest(ctx *fasthttp.RequestCtx) {
  log.WithFields(log.Fields{
    "path": string(ctx.Path()),
  }).Info("HandleRequest")

	switch string(ctx.Path()) {
	case "/info":
		h.infoHandler(ctx)
    return
	default:
    h.authHandler(ctx)
	}
}

func (h *AuthHandler) authHandler(ctx *fasthttp.RequestCtx) {
  path    := strings.Split(string(ctx.Path()), "/")

  if len(path) < 4 {
    ctx.SetStatusCode(fasthttp.StatusForbidden)
    return
  }

  ip      := path[2]
  user_id := path[3]

  // Individual limits
  ip_limit,   _   := strconv.Atoi(string(ctx.QueryArgs().Peek("ip_limit")))
  conn_limit, _   := strconv.Atoi(string(ctx.QueryArgs().Peek("conn_limit")))

  // General info
  stream_uri  := string(ctx.QueryArgs().Peek("stream_uri"))
  channel     := string(ctx.QueryArgs().Peek("channel"))

  if ip_limit <= 0 {
    ip_limit = Config.IpLimit
  }

  if conn_limit <= 0 {
    conn_limit = Config.ConnLimit
  }

  log.WithFields(log.Fields{
    "user_id": user_id,
    "ip": ip,
    "ip_limit": ip_limit,
    "conn_limit": conn_limit,
    "stream_uri": stream_uri,
    "channel": channel,
  }).Debug("authHandler")

  ips := userIps(user_id)
  conns := userConns(user_id)

  clients_count := len(ips)
  conns_count := len(conns)

  is_current_ip_active := false
  is_current_conn_active := false

  // Check if given client is already active/authorized
  for _, client_ip := range ips {
    if ip == client_ip {
      is_current_ip_active = true
    }
  }

  for _, client_conn := range conns {
    if stream_uri == client_conn {
      is_current_conn_active = true
    }
  }

  // Update connection hits counter
  hits := updateConnHits(stream_uri)

  log.WithFields(log.Fields{
    "stream_uri": stream_uri,
    "conns_count": conns_count,
    "hits": hits,
  }).Debug("Conns")

  // Limit number of IP connected per user_id
  if !is_current_ip_active && clients_count >= ip_limit {
    ctx.SetStatusCode(fasthttp.StatusForbidden)

    // publish blocked by IP event
    defer publishEvent("ip_blocked", user_id, ip, stream_uri)
    defer addMemberWithTimestampScore(IpBlockedKey, user_id)

    at := time.Now()
    fmt.Fprintf(blockedLog, "%s  Request blocked: IP address exceeded for subscription.\t%s=%s %s=%s\n", at.Format("2006-01-02 15:04:05"), "ip", ip, "uid", user_id)

    fmt.Fprintf(ctx, "Forbidden\n")
  // Limit Number of connection per user_id
  } else if !is_current_conn_active && hits > CONNHITS_TRESHOLD && conns_count >= conn_limit {
    ctx.SetStatusCode(fasthttp.StatusTooManyRequests)

    // publish blocked by connection event
    defer publishEvent("conn_blocked", user_id, ip, stream_uri)
    defer addMemberWithTimestampScore(ConnBlockedKey, user_id)
  } else {
    addUserIp(user_id, ip)
    ctx.SetStatusCode(fasthttp.StatusOK)

    // Update connection
    if hits > CONNHITS_TRESHOLD {
      defer addMemberWithTimestampScore(connsetKey(user_id), stream_uri)
    }

    // Publish aggragation data 
    defer publishEvent("request", user_id, ip, stream_uri)
    defer addMemberWithTimestampScore(ActiveKey, user_id)
    defer addMemberWithTimestampScore(UniqueIpKey, ip)
  }
}

func (h *AuthHandler) infoHandler(ctx *fasthttp.RequestCtx) {
  ctx.SetStatusCode(fasthttp.StatusOK)
  ctx.SetContentType("application/json")

  var cursor uint64
  clients := make(map[string]interface{})

  keys, _, err := RedisClient.Scan(cursor, "ipset*", 10000).Result()

  if err != nil {
    log.Errorf("Failed to fetch clients: %v", err)
  }

  for _, key := range keys {

    path      := strings.Split(key, ":")
    user_id   := path[1]
    user_ips  := userIps(user_id)

    if len(user_ips) > 0 {
      clients[user_id] = user_ips
    }
  }

  clients["*client_count"] = len(clients)
  body, err := json.Marshal(clients)

  if err != nil {
    log.Errorf("Failed to dump info to json : %v", err)
  }

  fmt.Fprintf(ctx, string(body))
}

// Generate key for ipset by userId
func ipsetKey(userId string) string {
  return strings.Join([]string{IPSET_PREFIX, userId}, "")
}

func connsetKey(userId string) string {
  return strings.Join([]string{CONNSET_PREFIX, userId}, "")
}

func connhitsKey(stream_uri string) string {
  return strings.Join([]string{CONNHITS_PREFIX, stream_uri}, "")
}

// NOTE: not is use currently
func updateAndGetActiveConnCount(user_id string, stream_uri string) int64 {
  if stream_uri == "" { return 0 }

  hits := updateConnHits(stream_uri)
  if hits > CONNHITS_TRESHOLD {
    addMemberWithTimestampScore(connsetKey(user_id), stream_uri)
  }
  return getLastMembersCount(connsetKey(user_id), Config.TimeFrame)
}

func getActiveConnCount(user_id string, stream_uri string) int64 {
  if stream_uri == "" { return 0 }
  return getLastMembersCount(connsetKey(user_id), Config.TimeFrame)
}

func updateConnHits(stream_uri string) int64 {
  if stream_uri == "" { return 0 }
  _, err := RedisClient.SetNX(connhitsKey(stream_uri), "0", CONNHITS_TTL*time.Second).Result()
  if err != nil {
    log.Fatalf("error opening file: %v", err)
  }
  hits, _ := RedisClient.Incr(connhitsKey(stream_uri)).Result()
  return hits
}

func addMemberWithTimestampScore(key string, value string) {
  RedisClient.ZAdd(key, redis.Z{
    Score:  float64(time.Now().Unix()),
    Member: value,
  })
  RedisClient.Expire(key, SET_TTL*time.Second)
  if rand.Float32() < CleanupProbability {
    removeExpiredMemebers(key)
  }
}

func removeExpiredMemebers(key string) {
  now := time.Now()
  to    := now.Add(time.Duration(-CleanupOlderThan)*time.Second)
  RedisClient.ZRemRangeByScore(key, "0", strconv.FormatInt(to.Unix(), 10))
}

func getLastMembers(key string, duration int) []string {
  now := time.Now()
  from  := now.Add(time.Duration(-duration)*time.Second)
  to    := now.Add(time.Duration(duration)*time.Second)

  return RedisClient.ZRangeByScore(key, redis.ZRangeBy{
    Min: strconv.FormatInt(from.Unix(), 10),
    Max: strconv.FormatInt(to.Unix(), 10),
  }).Val()
}

func getLastMembersCount(key string, duration int) int64 {
  now := time.Now()
  from  := now.Add(time.Duration(-duration)*time.Second)
  to    := now.Add(time.Duration(duration)*time.Second)

  return RedisClient.ZCount(key,
    strconv.FormatInt(from.Unix(), 10),
    strconv.FormatInt(to.Unix(), 10),
  ).Val()
}

// Return IP lists for given userId
func userIps(userId string) []string {
  now := time.Now()
  from  := now.Add(time.Duration(-Config.TimeFrame)*time.Second)
  to    := now.Add(time.Duration(Config.TimeFrame)*time.Second)

  ipsRange := RedisClient.ZRangeByScore(ipsetKey(userId), redis.ZRangeBy{
    Min: strconv.FormatInt(from.Unix(), 10),
    Max: strconv.FormatInt(to.Unix(), 10),
  }).Val()

  log.WithFields(log.Fields{
    "from": from.Unix(),
    "to":   to.Unix(),
    "ipsetKey": ipsetKey(userId),
    "userId": userId,
    "ips": ipsRange,
  }).Info("userIps")

  return ipsRange
}

// Return connection lists for given userId
func userConns(userId string) []string {
  now := time.Now()
  from  := now.Add(time.Duration(-Config.TimeFrame)*time.Second)
  to    := now.Add(time.Duration(Config.TimeFrame)*time.Second)

  connsRange := RedisClient.ZRangeByScore(connsetKey(userId), redis.ZRangeBy{
    Min: strconv.FormatInt(from.Unix(), 10),
    Max: strconv.FormatInt(to.Unix(), 10),
  }).Val()

  log.WithFields(log.Fields{
    "from": from.Unix(),
    "to":   to.Unix(),
    "userId": userId,
    "ips": connsRange,
  }).Info("userConns")

  return connsRange
}

// Add user ip to sorted set with rank as current timestamp
func addUserIp(userId string, ip string) {
  log.WithFields(log.Fields{
    "userId": userId,
    "ip": ip,
  }).Debug("addUserId")

  now := time.Now()
  RedisClient.ZAdd(ipsetKey(userId), redis.Z{
    Score:  float64(now.Unix()),
    Member: ip,
  })
  // Arbitrarily set expire time for ipset (selfcleanup)
  RedisClient.Expire(ipsetKey(userId), IPSET_TTL*time.Second)
}


func publishEvent(eventType string, userId string, ip string, stream_uri string) {
  eventMsg := map[string]interface{} {
    "event":      eventType,
    "user_id":    userId,
    "stream_uri": stream_uri,
    "ip":         ip,
    "at":         int32(time.Now().Unix()),
  }
  msg, _ := json.Marshal(eventMsg)
  RedisClient.Publish(PublishEventsPipe, msg)
}


var blockedLog *os.File

func main() {
  //log.SetLevel(log.DebugLevel)

  logfile, err := os.OpenFile("auth.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)

  if err == nil {
   log.SetOutput(logfile)
  } else {
   log.Error("Failed to log to file, using default stderr")
  }

  blockedLog, err = os.OpenFile("blocked.log", os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
  if err != nil {
    log.Fatalf("error opening file: %v", err)
  }

  rand.Seed(time.Now().UTC().UnixNano())

  Config.readConf()

  log.WithFields(log.Fields{
    "IpLimit": Config.IpLimit,
    "UserLimit":     Config.UserLimit,
    "TimeFrame":  Config.TimeFrame,
  }).Info("Config")

  authHandler := &AuthHandler{}

  fasthttp.ListenAndServe(":9000", authHandler.HandleRequest)
}
